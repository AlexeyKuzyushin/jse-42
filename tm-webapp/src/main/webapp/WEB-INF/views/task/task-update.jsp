<jsp:include page="../include/_header.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<h1>TASK UPDATE</h1>

<form:form action="/tasks/update/${task.id}" method="POST" modelAttribute="task">
    <form:input type="hidden" path="id"/>
    <p>
        <div style="margin-bottom: 5px">NAME:</div>
        <div><form:input type="text" path="name"/></div>
    </p>
    <p>
        <div style="margin-bottom: 5px">DESCRIPTION:</div>
        <div><form:input type="text" path="description"/></div>
    </p>
    <button type="submit">SAVE TASK</button>
</form:form>

<jsp:include page="../include/_footer.jsp" />