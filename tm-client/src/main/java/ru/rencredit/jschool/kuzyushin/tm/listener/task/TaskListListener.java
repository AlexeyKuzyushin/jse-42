package ru.rencredit.jschool.kuzyushin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.TaskDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.TaskSoapEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;
import ru.rencredit.jschool.kuzyushin.tm.service.SessionService;

import java.util.List;

@Component
public final class TaskListListener extends AbstractListener {

    @NotNull
    private final TaskSoapEndpoint taskSoapEndpoint;

    @NotNull
    private final SessionService sessionService;

    @Autowired
    public TaskListListener(
            final @NotNull TaskSoapEndpoint taskSoapEndpoint,
            final @NotNull SessionService sessionService
    ) {
        this.taskSoapEndpoint = taskSoapEndpoint;
        this.sessionService = sessionService;
    }

    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list";
    }

    @Override
    @EventListener(condition = "@taskListListener.name() == #event.command")
    public void handler(final ConsoleEvent event) {
        System.out.println("[LIST TASKS]");
        sessionService.setListCookieRowRequest(taskSoapEndpoint);
        @Nullable final List<TaskDTO> tasksDTO = taskSoapEndpoint.findAllTasks();
        int index = 1;
        for (TaskDTO taskDTO: tasksDTO) {
            System.out.println(index + ". " + taskDTO.getName());
            index++;
        }
        System.out.println("[OK]");
    }
}
